using System;
using System.Collections.Generic;
using FluentAssertions;
using Xunit;

namespace PokerHands.Test
{
    public class PokerHandsTests
    {
        [Fact]
        public void Constructor_NullArgs_ThrowsException()
        {
            // Arrange + Act + Assert

            ((Action) (() => new Hand(null))).Should().Throw<ArgumentNullException>();
        }


        [Fact]
        public void Given_a_hand_of_cards_returns_correct_rank()
        {
            // Arrange
            var SUT = new Hand(new List<Card>());
            // Act
            var result = SUT.GetRank();

            // Assert
            result.Should().Be(Rank.HighCard);
        }

        [Fact]
        public void Give_a_royalflush_getRank_returns_royalFlush()
        {
            // Arrange
            List<Card> cards = new List<Card>
            {
                new Card(),
            }
            
            var SUT = new Hand(cards);
            // Act
            var result = SUT.GetRank();

            // Assert
            result.Should().Be(Rank.RoyalFlush);
        }
    }

    public class Hand
    {
        List<Card> _cards;

        public Hand(List<Card> cards)
        {
            _cards = cards ?? throw new ArgumentNullException("cards");
        }

        public Rank GetRank()
        {
            return Rank.HighCard;
        }
    }

    public enum Rank
    {
        HighCard,
        OnePair,
        TwoPairs,
        ThreeOfAKind,
        Straight,
        Flush,
        FullHouse,
        FourOfAKind,
        StraighFlush,
        RoyalFlush
    }

    public class Card
    {
        Suit suit;
        int value;
    }

    internal enum Suit
    {
        Hearts,
        Clubs,
        Diamonds,
        Spades
    }
}
